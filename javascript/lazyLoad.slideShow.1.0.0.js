;
(function ($, undefined) {
    /**
     * jQuery plugin to create a responsive banner rotator,
     * it can be used as a background rotator too. this slideshow
     * uses lazy load to get the images.
     *
     * @version 1.0.1
     * @created 18/08/2014
     * @author John L. Diaz (graphicsinsite@hotmail.com)
     */
    $.fn.slideShow = function (options) {

        // set up default options
        var defaults = {
            object: {},
            interval: 10000,
            useAsBackground: false,
            fullScreen: false,
            cssClass: '',
            containerId: 'slideshow-hidden-container',
            selectorsClass: 'slideshow-selector-buttons ui-helper-clearfix',
            selectorsId: 'slideshow-selector-buttons',
            selectedClass: 'selected',
            cursorPosition: 0,
            count: 0,
            images: [],
            imagePath: '',
            container: {},
            loadedImages: [],
            hiddenContainer: {},
            timeInterval: null
        };

        var settings = $.extend({}, defaults, options);

        settings.object = $(this);

        if (settings.images.length === 0) {
            alert('Images neeed to initialize the gallery');
            return;
        }
        settings.count = settings.images.length;

        var slideShow = {
            swap: function (container) {
                if(!$("#" + settings.containerId).is(":visible")) {
                    $("#" + settings.containerId).fadeIn();
                }
                var found = $("#" + settings.containerId).find(":visible");
                if(found.length === 0) {
                    container.fadeIn(function () {
                        $(this).attr('data-active', true);
                    });
                    return;
                }
                found.fadeOut('slow', function() {
                    container.fadeIn(3000);
                });
            },
            animate: function (element, index) {
                if (typeof index === 'undefined') {
                    index = 1;
                }
                if (!element.is(':animated')) {
                    element.animate({ 'opacity': index }, 500);
                    return;
                }
                element.animate({ opacity: index }, 500);
            },
            buildSelectors: function () {
                if ($("#" + settings.selectorsId).length === 0) {
                    var self = this;
                    var selectors = jQuery('<ul/>', {
                        'class': settings.selectorsClass,
                        'id': settings.selectorsId
                    }).mouseenter(function () {
                        self.animate($(this), 1);
                    }).mouseleave(function () {
                        self.animate($(this), 0.4);
                    });
                    $.each(settings.images, function (key, image) {
                        var selector = jQuery('<li/>', {
                            'data-index': key
                        }).bind('click', function () {
                            self.loadImage($(this).attr('data-index'));
                            clearInterval(settings.timeInterval);
                        });
                        selectors.append(selector);
                    });
                    settings.object.append(selectors);
                    var left = (settings.object.width() / 2) - (selectors.width() / 2);
                    if (!settings.useAsBackground) {
                        settings.object.css({'position': 'relative'});
                        selectors.css({
                            'position': 'absolute',
                            'bottom': '5px',
                            'left': left
                        });
                    } else {

                        selectors.css({'left': left, 'top':20});
                    }
                }

            },
            highlightSelector: function () {
                $("#" + settings.selectorsId + " > li").removeClass(settings.selectedClass);
                var current = $("#" + settings.selectorsId).find("[data-index='" + settings.cursorPosition + "']");
                if (current.length > 0) {
                    current.addClass(settings.selectedClass);
                }
            },
            loadImage: function (index) {

                if (typeof settings.images[index] !== 'undefined') {
                    var self = this;
                    var container = $("#" + settings.containerId + index);
                    if(container.length > 0) {
                        var imagePath = settings.imagePath + settings.images[index];
                        if (jQuery.inArray(imagePath, settings.loadedImages) == -1) {
                            container.attr('src', imagePath).load(function() {
                                if (this.complete && typeof this.naturalWidth != "undefined" && this.naturalWidth !== 0) {
                                    self.swap(container);
                                    settings.loadedImages.push(imagePath);
                                }
                            })
                        } else {
                            self.swap(container);
                        }
                        this.highlightSelector();

                        if (settings.cursorPosition === (settings.count - 1)) {
                            settings.cursorPosition = 0;
                            return;
                        }
                        settings.cursorPosition = settings.cursorPosition + 1;
                    }

                }
            },

            loadImages: function () {
                var self = this;
                if ($("#" + settings.containerId).length === 0) {
                    settings.hiddenContainer = $("<div/>", {
                        'id': settings.containerId,
                        'css': {
                            'display': 'none'
                        }
                    });

                    if (settings.useAsBackground) {
                        settings.hiddenContainer.css({
                            'position': 'absolute',
                            'width': '100%',
                            'max-height': '100%',
                            'left': 0,
                            'top': 0,
                            'z-index': '-1'
                        });
                        if(settings.fullScreen) {
                            settings.hiddenContainer.css({
                                'height': '100%'
                            });
                        }
                    }
                    settings.hiddenContainer.appendTo(settings.object);

                } else {
                    settings.hiddenContainer = $("#" + settings.containerId);
                }

                $.each(settings.images, function (key, value) {
                    var imgContainer = jQuery(
                      '<img/>', {
                            'id': settings.containerId + key,
                            'css': {
                                'display': 'none'
                            },
                            'data-loaded': false
                        }
                    );
                    if(settings.useAsBackground) {
                        imgContainer.css({'width': '100%'});
                    }
                    settings.hiddenContainer.append(imgContainer);
                });
                self.loadImage(0);

                if (settings.count === 1) {
                    return;
                }
                this.buildSelectors();
                this.timeInterval = setInterval(function () {
                    if (typeof settings.images[settings.cursorPosition] !== 'undefined') {
                        self.loadImage(settings.cursorPosition);
                    }
                }, settings.interval);

            },
            init: function (object) {
				var querySelector = document.querySelector("img");
				if (querySelector) {
					querySelector.addEventListener("load", function (event, image) {
						if (typeof $(this).attr('src') !== 'undefined') {
							settings.loadedImages.push($(this).attr('src'));
						}
					});
				}

                this.loadImages();
                this.animate($("#" + settings.selectorsId), 0.4);

                if (object.selector === 'body') {
                    return;
                }
                $(object).find('img').css({'max-width': '100%'});
            }
        }

        slideShow.init(this);
    };
	

})(jQuery);