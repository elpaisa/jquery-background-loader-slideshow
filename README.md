# README #

This repository is for the jQuery Background Loader Slideshow plugin.

### What is this repository for? ###

* Any contributions are welcome
* Version 1.0.0

### How do I get set up? ###

* If you use git, just clone this repository to your own machine. To setupe GIT for Windows, here is the url: [http://msysgit.github.io/](http://msysgit.github.io/) , for linux users: [http://git-scm.com/download/linux](http://git-scm.com/download/linux) then
just use this command: git clone https://elpaisa@bitbucket.org/elpaisa/jquery-background-loader-slideshow.git


### Contribution guidelines ###

* Anyone can contribute as long as he/she keeps the plugin responsive
* Any new code block must be reusable.
* Use the same coding style of the plugin.
* Code review

### Who do I talk to? ###

* Repo owner or admin: John Leyton Diaz (graphicsinsite@hotmail.com)
* Other community or team contact